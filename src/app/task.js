/**
 * This file is part of the noggin package.
 *
 * (c) Rafał Lorenz <vardius@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
export class Task {
    constructor(title, type, date, body) {
        this.title = title;
        this.type = type;
        this.date = date;
        this.body = body;
    }

    getValue(property) {
        switch (property) {
            case '{{title}}':
                return this.title;
            case '{{date}}':
                return this.date;
            case '{{body}}':
                return this.body;
            case '{{type}}':
                return this.type;
        }
    }
}
