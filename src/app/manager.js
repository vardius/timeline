/**
 * This file is part of the noggin package.
 *
 * (c) Rafał Lorenz <vardius@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import {Task} from "./task";

export class TaskManager {
    getTasks() {
        let tasks = {};
        let data = localStorage.getItem('tasks');
        if (data != null) {
            tasks = JSON.parse(data);
            for (let i in tasks) {
                if (tasks.hasOwnProperty(i)) {
                    tasks[i] = $.extend(new Task(), tasks[i]);
                }
            }
        }
        return tasks;
    }

    setTasks(tasks) {
        tasks = this.sortTasks(tasks);
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    sortTasks(tasks) {
        return Object.keys(tasks).sort((a, b) => new Date(a) - new Date(b)).reduce((r, k) => (r[k] = tasks[k], r), {});
    }

    add(title, type, date, body) {
        let tasks = this.getTasks();
        tasks[date] = new Task(title, type, date, body);
        tasks = this.sortTasks(tasks);
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }

    remove(key) {
        let tasks = this.getTasks();
        delete tasks[key];
        localStorage.setItem('tasks', JSON.stringify(tasks));
    }
}
