/**
 * This file is part of the noggin package.
 *
 * (c) Rafał Lorenz <vardius@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
export function onScroll(event) {
    if (event.preventDefault) {
        event.preventDefault();
    }
    $('.timeline > li').each(function (index) {
        let mrg = parseInt($(this).css('margin-bottom'));
        mrg += event.deltaY < 0 ? -10 : 10;
        mrg = mrg < 30 ? 30 : mrg;
        $(this).css('margin-bottom', mrg + 'px');
    });
}

export function onAdd() {
    let title = document.getElementById('taskTitle').value;
    let date = document.getElementById('taskDate').value;
    let body = document.getElementById('taskBody').value;
    let type = document.getElementById('taskType').value;

    document.getElementById('taskTitle').value = '';
    document.getElementById('taskDate').value = '';
    document.getElementById('taskBody').value = '';

    if (!title || !body || !date || !type) {
        return false;
    }

    this.manager.add(title, type, date, body);

    this.showTimeline();

    return false;
}

export function onRemove(event) {
    let key = $(event.target).closest('.timeline-panel').data('key');
    this.manager.remove(key);

    this.showTimeline();

    return false;
}