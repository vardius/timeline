/**
 * This file is part of the noggin packagevent.
 *
 * (c) Rafał Lorenz <vardius@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source codevent.
 */
export class Draggable {
    constructor(manager, callback) {
        this.selected = null;
        this.manager = manager;
        this.callback = callback;
    }

    bindForClass(selector) {
        let draggable = document.getElementsByClassName(selector);
        for (let i = 0; i < draggable.length; i++) {
            draggable[i].addEventListener('dragstart', this.dragStart.bind(this), false);
            draggable[i].addEventListener('dragenter', this.dragEnter.bind(this), false);
            draggable[i].addEventListener('dragover', this.dragOver.bind(this), false);
            draggable[i].addEventListener('dragleave', this.dragLeave.bind(this), false);
            draggable[i].addEventListener('drop', this.drop.bind(this), false);
            draggable[i].addEventListener('dragend', this.dragEnd.bind(this), false);
        }
    }

    dragStart(event) {
        event.dataTransfer.effectAllowed = 'move';
        let $panel = $(event.target).closest('.timeline-panel');
        event.dataTransfer.setData('source', $panel.data("key"));
        this.selected = event.target;
        event.target.classList.add('moving');
    }

    dragOver(event) {
        if (event.preventDefault) {
            event.preventDefault();
        }
        event.dataTransfer.dropEffect = 'move';
        return false;
    }

    dragEnter(event) {
        event.target.classList.add('over');
    }

    dragLeave(event) {
        event.target.classList.remove('over');
    }

    drop(event) {
        if (event.stopPropagation) {
            event.stopPropagation();
        }

        if (this.selected != event.target) {
            let source = event.dataTransfer.getData('source');
            let $panel = $(event.target).closest('.timeline-panel');
            let target = $panel.data("key");

            let tasks = this.manager.getTasks();
            let sourceTask = tasks[source];
            let sourceTaskDate = new Date(sourceTask.date);

            delete tasks[source];

            sourceTaskDate.setDate(new Date(tasks[target].date).getDate() + 1);
            let newKey = sourceTaskDate.getFullYear() + '-' + (sourceTaskDate.getMonth() + 1) + '-' + sourceTaskDate.getDate();
            sourceTask.date = newKey;
            tasks[newKey] = sourceTask;

            this.manager.setTasks(tasks);
        }
        return false;
    }

    dragEnd(event) {
        this.callback();
    }
}
