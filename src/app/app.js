/**
 * This file is part of the noggin package.
 *
 * (c) Rafał Lorenz <vardius@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import {TaskManager} from "./manager";
import {Draggable} from "./drag";
import {Task} from "./task";
import {onScroll, onAdd, onRemove} from "./events";

export default class App {
    constructor() {
        this.manager = new TaskManager();
        this.draggable = new Draggable(this.manager, this.showTimeline.bind(this));

        this.init();
    }

    init() {
        let tasks = this.manager.getTasks();
        if (Object.keys(tasks).length < 1) {
            tasks = {
                "2016-10-1": new Task("Bath a Cat!", "info", "2016-10-1", '<img src="http://kingofwallpapers.com/random-image/random-image-001.jpg" class="img-rounded"> Lorem ipsum dolor sit amet tellus. Suspendisse sed massa. Nunc a nulla. Morbi mauris vitae purus. Pellentesque tellus. Aliquam erat a nunc. Vivamus volutpat at, ligula. Fusce convallis varius. Morbi vel libero. Aliquam eros. Sed egestas a, fermentum varius. Cras sit amet neque ultrices volutpat. Curabitur et ultrices posuere elementum vitae, cursus sed, viverra sem tincidunt wisi, tempor tincidunt dictum ut, pulvinar nec, accumsan at, velit. Vivamus metus gravida elit. Vestibulum ante ligula eget sem eu wisi. Mauris luctus eget, neque.'),
                "2016-10-3": new Task("Walk a Dog", "success", "2016-10-3", '<img src="http://www.shockmansion.com/wp-content/myimages/2016/03/rr231.jpg" class="img-circle"> <p>Lorem ipsum dolor sit amet dolor. In et malesuada pretium. Vivamus sem vestibulum ipsum dolor auctor metus. Maecenas ante. Morbi massa quis ante. Maecenas pretium, ipsum adipiscing non, ipsum. Donec eleifend lectus vulputate tempor diam. Morbi massa lacinia eget, accumsan ullamcorper nec, bibendum tellus, quis mauris. Nullam.</p>'),
                "2016-10-4": new Task("Cut Some Trees", "danger", "2016-10-4", '<img src="https://www.moooi.com/sites/default/files/styles/large/public/product-images/random_detail.jpg?itok=ErJveZTY" class="img-thumbnail"> Lorem ipsum dolor sit amet tempus urna ligula eget felis adipiscing ac, cursus wisi at augue sit amet mi porttitor a, diam. In justo semper sollicitudin. Nullam vulputate augue, vel dolor. Mauris a velit vitae libero. Pellentesque habitant morbi tristique id, lacinia varius dui. Integer mi mauris, volutpat ut, fermentum laoreet, lacus nec eros. Morbi egestas, dui quis justo. Aenean interdum malesuada. Nullam risus at nulla. Curabitur imperdiet, nisl erat ornare laoreet. Morbi quis.'),
                "2016-10-5": new Task("Task Title", "warning", "2016-10-5", 'Lorem ipsum dolor sit amet ligula. Vivamus ut orci luctus at, volutpat id, wisi. Aenean ipsum dolor lorem, at est eu eros. Mauris lobortis condimentum ultricies, hendrerit wisi. Praesent vitae est sem, vulputate wisi placerat urna quam, ultrices et, varius vehicula. Etiam dapibus eget, dui. In turpis id fringilla faucibus, convallis purus. Integer magna diam, tincidunt nonummy, tellus vulputate eu, ullamcorper orci imperdiet ut, neque. Sed porta neque, condimentum nec, sem. Nulla ac.'),
                "2016-10-6": new Task("Task Title 2", "primary", "2016-10-6", 'Lorem ipsum dolor sit amet metus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per conubia nostra, per inceptos hymenaeos. Maecenas bibendum risus. Nam quis leo. Quisque arcu sed nulla pellentesque at, accumsan sit amet, neque. Sed posuere a, sodales libero. Duis non enim fringilla mauris. Nullam libero dolor, luctus leo eros, sagittis nibh consectetuer adipiscing risus sit amet mauris non eros. Fusce urna vitae massa sit amet eros.'),
                "2016-10-7": new Task("Task Title 3", "info", "2016-10-7", 'Lorem ipsum dolor sit amet wisi. Morbi sit amet tellus. Cras eu aliquam erat. Pellentesque nunc interdum dapibus aliquam. In urna. Nunc in tellus sit amet, accumsan at, molestie ultricies tincidunt, diam turpis egestas. Praesent feugiat. Cum sociis natoque penatibus et neque. Sed dignissim enim ac elit justo nulla, at lacus a leo ac eros sed libero. Suspendisse nulla facilisis.'),
                "2016-10-8": new Task("Task Title 4", "info", "2016-10-8", 'Lorem ipsum dolor sit amet sapien magna ac eros ut metus nonummy condimentum sem tincidunt wisi, sed enim vulputate tempus nulla. Vivamus nec ante. Curabitur eu neque ultrices posuere egestas vitae, fringilla sit amet metus.'),
                "2016-10-9": new Task("Task Title 5", "info", "2016-10-9", 'Lorem ipsum dolor sit amet elit. Aliquam semper. Praesent quis ipsum. Integer est. Aliquam ante id eros. Vestibulum consectetuer vestibulum in, consequat non, feugiat pulvinar. Nulla euismod, volutpat a, dui. In hac habitasse platea dictumst. Nunc ut tellus dolor sit amet lacus. Vestibulum ante ipsum et cursus wisi.'),
                "2016-10-10": new Task("Task Title 6", "info", "2016-10-10", 'Lorem ipsum dolor sit amet turpis et magnis dis parturient montes, nascetur ridiculus mus. Praesent est a felis. Phasellus vitae turpis. Fusce tristique, augue varius vitae, egestas sit amet libero in augue. Donec magna felis auctor est, vel risus. Sed dignissim. Etiam vel tortor. Nullam in quam. Quisque ultricies lobortis dapibus, ultricies vitae, arcu. Sed eget sem. Nulla imperdiet sed, posuere suscipit id, elit. Aenean mollis luctus. Sed placerat tempus, urna a velit lectus dapibus non, quam. Aliquam fringilla, massa. Nulla nec ante.'),
                "2016-10-11": new Task("Task Title 7", "info", "2016-10-11", 'Lorem ipsum dolor sit amet augue. Donec elementum vitae, lacinia ut, turpis. Duis neque sollicitudin mauris turpis, et ultrices libero adipiscing elit. In hac habitasse platea dictumst. Suspendisse vitae sapien eros, sagittis sed, dapibus vel, ornare at, vehicula vitae, velit. Donec mi. Curabitur adipiscing wisi vel urna mattis ac, ante. Donec non luctus et netus et lorem. Pellentesque malesuada fames ac massa. Nulla.'),
                "2016-10-12": new Task("Task Title 8", "info", "2016-10-12", 'Lorem ipsum dolor sit amet ipsum primis in dui convallis aliquam quis, venenatis augue eget orci luctus bibendum. Nulla ligula non nibh. Quisque lobortis, varius eu, elit. Mauris pretium quis, lacinia sit amet, nonummy velit nec ligula enim eu dui ligula, nonummy lacinia quam elit sed ante ante, luctus euismod. Donec commodo, tincidunt fermentum. Aliquam ultricies a, mauris. Pellentesque habitant morbi tristique luctus nibh, interdum ligula tortor eros, rhoncus gravida. Pellentesque.'),
                "2016-10-13": new Task("Task Title 9", "info", "2016-10-13", 'Lorem ipsum dolor sit amet neque. Phasellus at eros. Sed pulvinar at, pretium quis, lorem. Pellentesque eu dui sodales in, consequat non, ipsum. Sed lobortis velit. Integer aliquet malesuada, diam justo, posuere cubilia Curae, Nulla sed massa non nonummy nisl nulla ut nibh. Morbi consequat urna semper egestas. Cras est ullamcorper ligula nulla, accumsan at, vehicula ut, blandit eu, purus. Pellentesque euismod, sapien eros, varius et, mollis ut, faucibus a, volutpat quam eros sed interdum rhoncus, dui porta ut, nunc. Suspendisse fermentum, dui quis enim consectetuer adipiscing elit. Sed hendrerit nunc fringilla eget.')
            };
            this.manager.setTasks(tasks);
        }
        this.showTimeline();
        this.subscribeEvents();
    }

    subscribeEvents() {
        $('[data-toggle="tooltip"]').tooltip();

        let timeline = document.getElementsByClassName('timeline').item(0);
        if (timeline) {
            timeline.addEventListener('wheel', onScroll);
        }

        let submitBtn = document.getElementById('submitTask');
        if (submitBtn) {
            submitBtn.addEventListener('click', onAdd.bind(this));
        }
    }

    showTimeline() {
        let $timeline = $(".timeline");
        $timeline.html("");
        this.resetCounter();

        let tasks = this.manager.getTasks();
        let lastTask = null;

        for (let i in tasks) {
            if (tasks.hasOwnProperty(i)) {
                if (lastTask) {
                    this.addEmptyDays(new Date(lastTask.date), new Date(tasks[i].date));
                }

                let counter = document.getElementById('counter-' + tasks[i].type);
                if (counter) {
                    counter.innerHTML = (parseInt(counter.innerHTML) + 1) + ' Tasks';
                }

                let template = require("./../public/task.html");
                template = template.replace(/{{title}}|{{date}}|{{body}}|{{type}}/gi, prop => tasks[i].getValue(prop));
                $timeline.append(template);
                lastTask = tasks[i];
            }
        }
        this.draggable.bindForClass('timeline-panel');
        let closeBtns = document.getElementsByClassName('close');
        for (let i = 0; i < closeBtns.length; i++) {
            closeBtns[i].addEventListener('click', onRemove.bind(this));
        }
    }

    addEmptyDays(start, end) {
        start.setDate(start.getDate() + 1);
        for (; start < end; start.setDate(start.getDate() + 1)) {
            let template = require("./../public/day.html");
            template = template.replace('{{date}}', prop => start.getFullYear() + '-' + (start.getMonth() + 1) + '-' + start.getDate());
            $(".timeline").append(template);
        }
    }

    resetCounter() {
        let counters = ['primary', 'success', 'danger', 'warning', 'info'];
        for (let i = 0; i < counters.length; i++) {
            let counter = document.getElementById('counter-' + counters[i]);
            if (counter) {
                counter.innerHTML = 0 + ' Tasks';
            }
        }
    }
};
