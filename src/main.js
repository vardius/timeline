/**
 * This file is part of the noggin package.
 *
 * (c) Rafał Lorenz <vardius@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
import App from './app/app.js';
import "./styles/app.scss";

const app = new App();